package com.nirmal.anivale;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class EnlargeActivity extends AppCompatActivity {

    ImageView enlargeOne;
    TextView txtClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enlarge);

        enlargeOne = (ImageView) findViewById(R.id.imageviewEnlarge);
        txtClose = (TextView) findViewById(R.id.closeWindow);

        //to close enlarged image activity
        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //calling function to show the correspondent enlarge image
        showImage();

    }

    public void showImage()
    {
        String url = getIntent().getExtras().getString("enlargeUrl");
        Glide
                .with(this)
                .load(url)
                .centerCrop()
                .crossFade()
                .into(enlargeOne);
    }
}
