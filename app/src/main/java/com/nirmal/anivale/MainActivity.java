package com.nirmal.anivale;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    String URL = "https://pixabay.com/api/?key=20050141-1d4bd7b458becec6681deda26&q=yellow+flowers&image_type=photo";
    GridView imageGridView;
    GridAdapter myGridAdapter;
    ArrayList<ImageObject> imageObjects;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialization of gird and progress bar
        imageGridView = (GridView) findViewById(R.id.grid);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        //calling function to load data from server into grid
        getJsonData();


    }


    public void getJsonData() {

        RequestQueue queue;

        queue = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("MainActivity","API Response: "+response);
              //  Toast.makeText(MainActivity.this,response.toString(),Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.INVISIBLE);
                imageGridView.setVisibility(View.VISIBLE);



                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray allImageArray = jsonObject.optJSONArray("hits");


                    if(allImageArray != null && allImageArray.length() > 0) {

                         imageObjects = new ArrayList<>();
                        for (int i = 0; i < allImageArray.length(); i++) {
                            JSONObject jsonItem = allImageArray.optJSONObject(i);

                            imageObjects.add(new ImageObject(jsonItem));
                        }

                        Log.d("MainActivity","Size imageObject in main: "+imageObjects.size());

                        myGridAdapter = new GridAdapter(MainActivity.this, imageObjects);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageGridView.setAdapter(myGridAdapter);
                            }
                        });
                    }

                    imageGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            //Toast.makeText(getApplicationContext(),"pos: "+position,Toast.LENGTH_SHORT).show();

                            String url = "null";
                            for (int i=0;i<imageObjects.size();i++)
                            {
                                if(position == i)
                                {
                                    url = imageObjects.get(i).getEnlargeImageUrl();
                                }
                            }

                            Intent intent = new Intent(getApplicationContext(),EnlargeActivity.class);
                            intent.putExtra("enlargeUrl",url);
                            startActivity(intent);

                        }
                    });

                        //JSONArray imagesArray = jsonObjectHit.optJSONArray("previewURL");

                    //Log.d("MainActivity", "Sample test: "+imagesArray.get(0));

                    /*for (int i=0; i < jsonObjectHit.length(); i++)
                    {
                        imagesArray = jsonObjectHit.getString("previewURL");
                    }*/

                }catch (Exception e)
                {
                    e.printStackTrace();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error",error.toString());
            }
        });
        queue.add(request);

    }
}
