package com.nirmal.anivale;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ImageObject> imageObjects;
    ImageObject imageObject;

    private LayoutInflater mLayoutInflate;


    public GridAdapter (Context context, ArrayList<ImageObject> imageObjects){
        this.context = context;
        this.imageObjects = imageObjects;

        this.mLayoutInflate = LayoutInflater.from(context);
    }

    public int getCount() {
        if(imageObjects != null) return  imageObjects.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(imageObjects != null && imageObjects.size() > position) return  imageObjects.get(position);

        return null;
    }

    @Override
    public long getItemId(int position) {
        if(imageObjects != null && imageObjects.size() > position) return  imageObjects.get(position).getId();
        return 0;
    }

    //THe image array list is loaded into grid usinf glide
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        Log.d("MainActivity","one");
        if (convertView == null) {

            Log.d("MainActivity","two");
            viewHolder = new ViewHolder();

            convertView = mLayoutInflate.inflate(R.layout.imageitem, parent,
                    false);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
         imageObject = (ImageObject) getItem(position);
        if(imageObject != null) {
            Log.d("MainActivity","three : " + imageObject.getImageUrl());
            Glide
                    .with(context)
                    .load(imageObject.getImageUrl())
                    .centerCrop()
                    .crossFade()
                    .into(viewHolder.imageView);
        }

/*
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,EnlargeActivity.class);
                intent.putExtra("enlargeUrl",imageObject.getEnlargeImageUrl());
                context.startActivity(intent);
            }
        });
*/

        return convertView;
    }

    private class ViewHolder {
        public ImageView imageView;
    }
}
