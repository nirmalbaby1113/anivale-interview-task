package com.nirmal.anivale;

import org.json.JSONObject;

public class ImageObject {
    private int id;
    private String imageUrl,imageEnlargeURL;

    public ImageObject(){

    }

    public  ImageObject(JSONObject jsonObject){
        if(jsonObject == null) return;
        this.id = jsonObject.optInt("id");
        this.imageUrl = jsonObject.optString("previewURL");
        this.imageEnlargeURL = jsonObject.optString("largeImageURL");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getEnlargeImageUrl() {
        return imageEnlargeURL;
    }
}
